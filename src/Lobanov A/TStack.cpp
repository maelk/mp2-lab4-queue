#ifndef __TSTACK_CPP__
#define __TSTACK_CPP__
 
#include "TStack.h"
#include <iostream>
using namespace std;

int TStack::GetNextIndex(int index) 
{
    return ++index;
}

void TStack::Put(const TData& Val)
{
	if (pMem == nullptr) 
	{ 
		throw SetRetCode(DataNoMem); 
	}
	else if (IsFull())
	{
	    throw SetRetCode(DataFull);
	}
	else
	{
		Hi = GetNextIndex(Hi);
		pMem[Hi] = Val;
		DataCount++;
	}
}

TData TStack::Get(void)
{
	if (pMem == nullptr)	
		throw SetRetCode(DataNoMem);	
	else if (IsEmpty())	
		throw SetRetCode(DataEmpty);	
	else
	{
		DataCount--;
		return pMem[Hi--];
	}
}

void TStack::Print()
{
	if (DataCount == 0) { cout << "Stack is empty!"; }
	for (int i = 0; i < DataCount; i++)
	{
		cout << pMem[i] << " ";
	}
	cout << endl;
}

int TStack::IsValid()
{
	int res = 0;
	if (pMem == NULL)
	   res = 1;
	if ( MemSize < DataCount )
	   res += 2;
	return res;
}

#endif