﻿# Лабораторная работа №4. Очереди#

## 1. Введение ##

Лабораторная работа направлена на практическое освоение динамической структуры данных **Очередь**. С этой целью в лабораторной работе изучаются различные варианты структуры хранения очереди и разрабатываются методы и программы решения задач с использованием очередей. В качестве области приложений выбрана тема эффективной организации выполнения потока заданий на вычислительных системах.

Очередь характеризуется таким порядком обработки значений, при котором вставка новых элементов производится в конец очереди, а извлечение – из начала. Подобная организация данных широко встречается в различных приложениях. В качестве примера использования очереди предлагается задача разработки системы имитации однопроцессорной ЭВМ. Рассматриваемая в рамках лабораторной работы схема имитации является одной из наиболее простых моделей обслуживания заданий в вычислительной системе и обеспечивает тем самым лишь начальное ознакомление с проблемами моделирования и анализа эффективности функционирования реальных вычислительных систем.

**Очередь (англ. queue)**, – схема запоминания информации, при которой каждый вновь поступающий ее элемент занимает крайнее положение (*конец очереди*). При выдаче информации из очереди выдается элемент, расположенный в очереди первым (*начало очереди*), а оставшиеся элементы продвигаются к началу; следовательно, элемент, поступивший первым, выдается первым. 

## 2. Цели и задачи

Для вычислительной системы (ВС) с одним процессором и однопрограммным последовательным режимом выполнения поступающих заданий требуется разработать программную систему для имитации процесса обслуживания заданий в ВС. При построении модели функционирования ВС должны учитываться следующие основные моменты обслуживания заданий:

- генерация нового задания;
- постановка задания в очередь для ожидания момента освобождения процессора;
- выборка задания из очереди при освобождении процессора после обслуживания очередного задания.

По результатам проводимых вычислительных экспериментов система имитации должна выводить информацию об условиях проведения эксперимента (интенсивность потока заданий, размер очереди заданий, производительность процессора, число тактов имитации) и полученные в результате имитации показатели функционирования вычислительной системы, в т.ч.

- количество поступивших в ВС заданий;
- количество отказов в обслуживании заданий из-за переполнения очереди;
- среднее количество тактов выполнения заданий;
- количество тактов простоя процессора из-за отсутствия в очереди заданий для обслуживания.

Показатели функционирования вычислительной системы, получаемые при помощи систем имитации, могут использоваться для оценки эффективности применения ВС; по результатам анализа показателей могут быть приняты рекомендации о целесообразной модернизации характеристик ВС (например, при длительных простоях процессора и при отсутствии отказов от обслуживания заданий желательно повышение интенсивности потока обслуживаемых заданий и т.д.).

### План работы

- Реализация структуры хранения данных TQueue на основе имеующейся структуры TStack.
- Проверка работоспособности класса TQueue с помощью Google Test Framework.
- Реализация модели потока заданий TJobStream.
- Реализация модели имитации процессора TProc.

### Дополнительное задание:

- Реализовать две очереди, вместо одной.

## 3. Описание структуры данных Очередь

Напомним, что динамическая структура есть математическая структура, которой соответствует частично-упорядоченное (по включению) базовое множество **М**, операции вставки и удаления элементы которого являются структурами данных. При этом отношения включения индуцируются операциями преобразования структуры данных.

Таким образом, очередь есть динамическая структура, операции вставки и удаления переводят очередь из одного состояния в другое, при этом добавление новых элементов осуществляется в конец очереди, а извлечение – из начала очереди (дисциплина обслуживания «первым пришел – первым обслужен.

Важной задачей при реализации системы обслуживания очереди является выбор структуры хранения, обеспечивающей решение проблемы эффективного использования памяти без перепаковок и без использования связных списков (требующих дополнительных затрат памяти на указатели).

Как и в случае со стеком, в качестве структуры хранения очереди предлагается использовать одномерный (одноиндексный) массив, размещаемый в динамической области памяти. В связи с характером обработки значений, располагаемых в очереди, для указания хранимых в очереди данных необходимо иметь два указателя – на начало и конец очереди. Эти указатели увеличивают свое значение: один при вставке, другой при извлечении элемента. Таким образом, в ходе функционирования очереди может возникнуть ситуация, когда оба указателя достигнут своего наибольшего значения и дальнейшее пополнение очереди станет невозможным, несмотря на наличие свободного пространства в очереди. Одним из решений проблемы «движения» очереди является организация на одномерном массиве кольцевого буфера. Кольцевым буфером называется структура хранения, получаемая из вектора расширением отношения следования парой **p(an,a1)**. 

![Кольцевой буфер](/images/1.PNG)

Структура хранения очереди в виде кольцевого буфера может быть определена как одномерный (одноиндексный) массив, размещаемый в динамической области памяти и расположение данных в котором определяется при помощи следующего набора параметров:

- **pMem** – указатель на память, выделенную для кольцевого буфера,
- **MemSize** – размер выделенной памяти,
- **MaxMemSize** – размер памяти, выделяемый по умолчанию, если при создании кольцевого буфера явно не указано требуемое количество элементов памяти,
 
- **DataCount** – количество запомненных в очереди значений,
- **Hi** – индекс элемента массива, в котором хранится последний элемент очереди,
- **Li** – индекс элемента массива, в котором хранится первый элемент очереди.

![Кольцевой буфер (на массиве)](/images/2.PNG)

В связи с тем, что структура хранения очереди во многом аналогична структуре хранения стек, предлагается класс для реализации очереди построить наследованием от класса стек, описанного в лабораторной работе №3. При наследовании достаточно переопределить методы **Get** и **GetNextIndex**. В методе **Get** изменяется индекс для получения элемента (извлечение значений происходит из начала очереди), метод **GetNextIndex** реализует отношение следования на кольцевом буфере.


***TStack.h***
```c++
  #ifndef __TSTACK_H__
  #define __TSTACK_H__

  #include "tdataroot.h"

  class TStack : public TDataRoot
  {
  protected:
      int Hi;
      virtual int GetNextIndex(int index);
  public:
      TStack(int Size = DefMemSize) : TDataRoot(Size) { Hi = -1; }
      virtual void Put(const TData &Val);
      virtual TData Get(void);
      int Get_Size() { return MemSize; }
      void Print();
      virtual int IsValid();
  };

  #endif

```

***TStack.cpp***
```c++
  #ifndef __TSTACK_CPP__
  #define __TSTACK_CPP__
 
  #include "TStack.h"
  #include <iostream>
  using namespace std;

  int TStack::GetNextIndex(int index) 
  {
      return ++index;
  }

  void TStack::Put(const TData& Val)
  {
      if (pMem == nullptr) 
      { 
          throw SetRetCode(DataNoMem); 
      }
      else if (IsFull())
      {
          throw SetRetCode(DataFull);
      }
      else
      {
          Hi = GetNextIndex(Hi);
	  pMem[Hi] = Val;
          DataCount++;
      }
  }

  TData TStack::Get(void)
  {
      if (pMem == nullptr)	
          throw SetRetCode(DataNoMem);	
      else if (IsEmpty())	
	  throw SetRetCode(DataEmpty);	
      else
      {
	  DataCount--;
	  return pMem[Hi--];
      }
  }

  void TStack::Print()
  {
      if (DataCount == 0) { cout << "Stack is empty!"; }
	  for (int i = 0; i < DataCount; i++)
	  {
	     cout << pMem[i] << " ";
	  }
      cout << endl;
  }

  int TStack::IsValid()
  {
      int res = 0;
      if (pMem == NULL)
          res = 1;
      if ( MemSize < DataCount )
          res += 2;
      return res;
  }

  #endif
```
## 4. Реализация класса TQueue

### Описание алгоритма

Для работы с очередью предлагается реализовать следующие операции:

Методы, которые наследуются из класса **стек**.

- **Put** - добавить элемент;
- **IsEmpty** - проверить очередь на пустоту;
- **IsFull** - проверить очередь на полноту.

Методы, которые необходимо переопределить:

- **GetNextIndex** - реализует отношение следования на кольцевом буфере.
- **Get** - элемент извлекаем из начала очереди.


***TQueue.h***

```c++
  #ifndef __TQUEUE_H__
  #define __TQUEUE_H__

  #include "tstack.h"

  class TQueue : public TStack
  {
  protected:
     int Li; //индекс первого элемента
     virtual int GetNextIndex(int index);
  public:
     TQueue(int Size = DefMemSize) : TStack(Size) { Li = 0; }
     virtual TData Get (void);
  };
```
#endif

***TQueue.cpp***

```c++ 
  #ifndef __TQUEUE_CPP__
  #define __TQUEUE_CPP__

  #include "tqueue.h"

  int TQueue::GetNextIndex(int index)
  {
      return ++index % MemSize;
  }

  TData TQueue::Get(void)
  {
	  TData temp = -1;
	  if (pMem == nullptr)
	      throw SetRetCode(DataNoMem);
	  else if (IsEmpty())	
	      throw SetRetCode(DataEmpty);
	  else {
	      temp = pMem[Li];
	      Li = GetNextIndex(Li);
	      DataCount--;
	  }
          return temp;
  }

  #endif
```

## 5.Проверка работоспособности класса TQueue

### Тестовая программа для проверерки корректности работы очереди

```c++
  #include "tqueue.h"
  #include <iostream>
  #include "TProc.h"

  using namespace std;

  int main(int argc, char* argv[])
  {
	  TQueue st(20);
	  int temp;
	  cout << "Test QUEUE" << endl;
	  for (int i = 0; i < 20; i++)
	  {
		  st.Put(i);
		  cout <<  "Put element " << i << " CODE " << st.GetRetCode() << endl;
	  }
	  while (!st.IsEmpty()) 
	  {
		  temp = st.Get();
		  cout << " Get element " << temp << " CODE " << st.GetRetCode() << endl;
	  }
  }

```
### Тестирование с помощью Google Test Framework

```c++
***TestsQueue.cpp***

  #include "gtest.h"
  #include "tqueue.h"

TEST(TQueue, can_create_queue)
{
	ASSERT_NO_THROW(TQueue(2));
}

TEST(TQueue, cant_create_queue_with_negative_size)
{
	ASSERT_ANY_THROW(TQueue(-1));
}

TEST(TQueue, can_put_elem)
{
	TQueue q1(3);
	ASSERT_NO_THROW(q1.Put(1));
}

TEST(TQueue, cant_put_elem_in_full_queue)
{
	TQueue q1(3);
	q1.Put(1);
	q1.Put(2);
	q1.Put(3);
	ASSERT_ANY_THROW(q1.Put(4));
}

TEST(TQueue, can_get_elem)
{
	TQueue q1(3);
	int Elem1;
	q1.Put(1);
	Elem1 = q1.Get();
	EXPECT_EQ(Elem1, 1);
}
TEST(TQueue, queue_with_one_element_become_empty_after_getting_element)
{
	TQueue q1;
	q1.Put(1);
	q1.Get();

	EXPECT_TRUE(q1.IsEmpty());
}

TEST(TQueue, IsEmpty_works_correctly)
{
	TQueue q1(3);
	EXPECT_TRUE(q1.IsEmpty()); 
}

TEST(TQueue, get_returns_first_placed_elem)
{
	TQueue q1;
	int elem1 = 1, elem2 = 2, elem3 = 3, res;
	q1.Put(elem1); q1.Put(elem2); q1.Put(elem3);
	res = q1.Get();
	EXPECT_TRUE(res == elem1);
}

TEST(TQueue, IsFull_works_correctly)
{
	TQueue q1(3);
	q1.Put(1);
	q1.Put(2);
	q1.Put(3);
	EXPECT_TRUE(q1.IsFull());
}

TEST(TQueue, cant_get_elem_when_queue_is_empty)
{
	TQueue q1(3);
	ASSERT_ANY_THROW(q1.Get());
}

TEST(TQueue, ring_buffer_works_correctly)
{
        TQueue q1(3);
        q1.Put(1);
        q1.Put(2);
        q1.Put(3);
        q1.Get();
        q1.Get();
        q1.Put(4);
        q1.Put(5);
        EXPECT_EQ(1, q1.Get() == 3 && q1.Get() == 4 && q1.Get() == 5);
}
```

### Результат работы тестов:


![](https://bytebucket.org/maelk/mp2-lab4-queue/raw/c0b40482231d11b6dd561b10371f30523b159092/src/Lobanov%20A/screenTests.png)


### Результат работы тестовой программы:


![](https://bytebucket.org/maelk/mp2-lab4-queue/raw/c0b40482231d11b6dd561b10371f30523b159092/src/Lobanov%20A/progqueue.png)


## 6. Имитация модели вычислительной системы

### Описание алгоритма

Для моделирования момента появления нового задания можно использовать значение датчика случайных чисел. Если значение датчика меньше некоторого порогового значения **q1, 0<=q1<=1**, то считается, что на данном такте имитации в вычислительную систему поступает новое задание (тем самым параметр **q1** можно интерпретировать как величину, регулирующую интенсивность потока заданий – новое задание генерируется в среднем один раз за **(1/q1)** тактов).

Моделирование момента завершения обработки очередного задания также может быть выполнено по описанной выше схеме. При помощи датчика случайных чисел формируется еще одно случайное значение, и если это значение меньше порогового значения **q2, 0<=q2<=1**,то принимается, что на данном такте имитации процессор завершил обслуживание очередного задания и готов приступить к обработке задания из очереди ожидания (тем самым параметр **q2** можно интерпретировать как величину, характеризующую производительность процессора вычислительной системы – каждое задание обслуживается в среднем за **(1/q2)** тактов ).

Возможная простая схема имитации процесса поступления и обслуживания заданий в вычислительной системе состоит в следующем.

- Каждое задание в системе представляется некоторым однозначным идентификатором (например, порядковым номером задания).
- Для проведения расчетов фиксируется (или указывается в диалоге) число тактов работы системы.
- На каждом такте опрашивается состояние потока задач и процессор.
- Регистрация нового задания в вычислительной системе может быть сведена к запоминанию идентификатора задания в очередь ожидания процессора. Ситуацию переполнения очереди заданий следует понимать как нехватку ресурсов вычислительной системы для ввода нового задания (отказ от обслуживания).
- Для моделирования процесса обработки заданий следует учитывать, что процессор может быть занят обслуживанием очередного задания, либо же может находиться в состоянии ожидания (простоя).
- В случае освобождения процессора предпринимается попытка его загрузки. Для этого извлекается задание из очереди ожидания.
- Простой процессора возникает в случае, когда при завершении обработки очередного задания очередь ожидающих заданий оказывается пустой.
- После проведения всех тактов имитации производится вывод характеристик вычислительной системы:

 - количество поступивших в вычислительную систему заданий в течение всего процесса имитации;
 - количество отказов в обслуживании заданий из-за переполнения очереди – целесообразно считать процент отказов как отношение количества не поставленных в очередь заданий к общему количеству сгенерированных заданий, умноженное на 100%;
 - среднее количество тактов выполнения задания;
 - количество тактов простоя процессора из-за отсутствия в очереди заданий для обслуживания – целесообразно считать процент простоя процессора как отношение количества тактов простоя процессора к общему количеству тактов имитации, умноженное на 100%.

## Условия и ограничения

Сделаем следующие основные допущения:
1) При планировании очередности обслуживания заданий возможность задания приоритетов не учитывается.
2) Моменты появления новых заданий и моменты освобождения процессора рассматриваются как случайные события.

### Реализация класса *TJobStream*


***TJobStream.h***
```c++
  #ifndef __TJOBSTREAM_H__
  #define __TJOBSTREAM_H__

  #include <ctime>
  #include <cstdlib>

  class TJobStream
  {
  private:
      int q1; //вероятность появления нового задания (0-99)
      int Jbs; //кол-во заданий
  public:
      TJobStream(int Q1);
      bool GenerateJob(); //генерирует новое задание
      int GetTasks() const; //возвращает общее кол-во заданий
      int GetChance() const; //возвращает вероятность появления нового задания
  };

  #endif
```
***TJobStream.cpp***
```c++
  #ifndef __TJOBSTREAM_CPP__
  #define __TJOBSTREAM_CPP__

  #include "TJobStream.h"

  TJobStream::TJobStream(int Q1) 
  {
	  Jbs = 0;
	  q1 = Q1;
  }

  bool TJobStream::GenerateJob()
  {
   	  if (rand() % 100 < q1)
	  {
		  Jbs++;
		  return true;
	  }
	  return false;
  }

  int TJobStream::GetTasks() const
  {
	  return Jbs;
  }
  int TJobStream::GetChance() const
  {
	  return q1;
  }

  #endif
```

### Реализация класса *TProc*, использующего две очереди


***TProc.h***
```c++
  #ifndef __TPROC_H__
  #define __TPROC_H__

  #include <ctime>
  #include <cstdlib>
  #include "TJobStream.h"
  #include "tqueue.h"

  #define Max_Tacts 1000000

  enum T_Status { ON_WORK, FREE };

  class TProc
  {
  private:
      T_Status Status;
      TQueue que1;
      TQueue que2;
      TJobStream TJob;
      int ID; //номер текущего задания
      int q2; //вероятность выполнения текущего задания (0 - 99)
      int idletacts; //такты простоя
      int CountTasks; //выполненные задания
      int rejectcount; //пропущенные задания
  public:
      TProc(int Q1, int Q2, int _size1, int _size2);
      void Process();
      void PrintReport() const;
  };

  #endif
```

***TProc.cpp***

```c++
  #ifndef __TPROC_CPP__
  #define __TPROC_CPP__

  #include "TProc.h"
  #include <locale>
  #include <iostream>

  using namespace std;

  TProc::TProc(int  Q1, int Q2, int _size1,  int _size2) : TJob(Q1), q2(Q2), que1(_size1), que2(_size2)
  {
      ID = 0;
      idletacts = 0;
      CountTasks = 0;
      rejectcount = 0;
      srand(time(NULL));
      Status = FREE;
  }

  void TProc::Process()
  {
      for ( int i = 0; i < Max_Tacts; i++)
      {
          if (TJob.GenerateJob())
	  {
              ID++;
	      if(!(que1.IsFull()))	    
	          que1.Put(ID);		
	      else if (!(que2.IsFull()) && que1.IsFull()) 
		  que2.Put(ID);
	      else 
		  rejectcount++;   
	  }
	     
	  if (Status == ON_WORK )
	      if(rand() % 100 < q2)
		  Status = FREE;
	     
          if (Status == FREE)  
	  {
	      if (!(que1.IsEmpty()))
	      {    
	          que1.Get();
	          CountTasks++;
		  Status = ON_WORK;
	      }
	      else if (!(que2.IsEmpty()) && que1.IsEmpty())
	      {
	          que2.Get();
	          CountTasks++;
                  Status = ON_WORK;
	      }
	      else
	          idletacts++;
	      }
	  }   
  }
  void TProc::PrintReport() const
  {
      setlocale(LC_ALL, "rus");
	  double arr;
	  cout << endl;
	  cout << "Количество тактов процессора: " << Max_Tacts << endl;
	  cout << "Вероятность поступления нового задания: " << TJob.GetChance() << "%" << endl;
	  cout << "Вероятность выполнения текущего задания: " << q2 << "%" << endl;
	  cout << "Количество поступивших в вычислительную систему заданий в течение всего процесса имитации: " << TJob.GetTasks() << endl;
	  cout << "Количество заданий обработанных процессором: " << CountTasks << endl;
	  cout << "Количество заданий не обработанных процессором: " << TJob.GetTasks() - CountTasks << endl;
	  cout << "Количество отказов в обслуживании заданий из-за переполнения очереди: " << rejectcount << endl;
	  arr = (rejectcount * 100.0) / (float)TJob.GetTasks();
	  cout << "Процент отказов: " << arr << "%" << endl;
	  arr = (Max_Tacts - idletacts) / CountTasks;
	  cout << "Cреднее количество тактов на выполнение одного задания: " << arr << endl;
	  cout << "Количество тактов простоя: " << idletacts << endl;
	  arr = (idletacts * 100) / Max_Tacts;
	  cout << "Процент простоя: " << arr  << "%" << endl;
  }

  #endif
```

***QueueTestkit.cpp***

```c++
#include "tqueue.h"
#include <iostream>
#include "TProc.h"

using namespace std;

int main(int argc, char* argv[])
{
	TProc Pr(20, 25, 15, 15);
	Pr.Process();
	Pr.PrintReport();
	return 0;
}
```

### Результат работы программы

![](https://bytebucket.org/maelk/mp2-lab4-queue/raw/c0b40482231d11b6dd561b10371f30523b159092/src/Lobanov%20A/main.png)

## 7. Результаты исследования вычислительной системы

#### Исследования были произведены для 1 000 000 тактов, в работе использовались две очереди.
 
 1) При увеличении размера очереди уменьшается число отказов в обслуживании задачи из-за переполнения очереди, увеличивается число тактов простоя. Наиболее эффективно использовать очереди одинакового размера.
 
 2) При фиксированных значениях размера очередей и вероятности выполнения задания с ростом вероятности поступления нового задания увеличивается общее число заданий и процент отказов, уменьшается количество тактов простоя.

![](https://bytebucket.org/maelk/mp2-lab4-queue/raw/c0b40482231d11b6dd561b10371f30523b159092/src/Lobanov%20A/proc2.png)

 3) При фиксированных значениях размера очереди и вероятности поступления нового задания с ростом вероятности выполнения задачи на текущем такте возрастает число выполненных процессором заданий, уменьшается процент отказов в обслуживании и среднее число тактов, требующихся на выполнение конкретного задания, растет количество тактов простоя.

![](https://bytebucket.org/maelk/mp2-lab4-queue/raw/c0b40482231d11b6dd561b10371f30523b159092/src/Lobanov%20A/proc.png)


## 8. Выводы

 - В ходе выполнения данной работы были получены навыки работы со структурой данных очередь (Queue). 

 - В очередной раз был использован такой подход в ООП как наследование, это позволило существенно упростить разработку класса TQueue, наследника класса TStack. Работоспособность очереди была протестирована с помощью Google Test Framework, это позволило предотвратить большое количество ошибок.

 - Реализованный класс TQueue был использован при написании модели имитации выполнения задач вычислительной системой (процессором).




