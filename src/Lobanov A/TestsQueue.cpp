#include "gtest.h"
#include "tqueue.h"

TEST(TQueue, can_create_queue)
{
	ASSERT_NO_THROW(TQueue(2));
}

TEST(TQueue, cant_create_queue_with_negative_size)
{
	ASSERT_ANY_THROW(TQueue(-1));
}

TEST(TQueue, can_put_elem)
{
	TQueue q1(3);
	ASSERT_NO_THROW(q1.Put(1));
}

TEST(TQueue, cant_put_elem_in_full_queue)
{
	TQueue q1(3);
	q1.Put(1);
	q1.Put(2);
	q1.Put(3);
	ASSERT_ANY_THROW(q1.Put(4));
}

TEST(TQueue, can_get_elem)
{
	TQueue q1(3);
	int Elem1;
	q1.Put(1);
	Elem1 = q1.Get();
	EXPECT_EQ(Elem1, 1);
}
TEST(TQueue, queue_with_one_element_become_empty_after_getting_element)
{
	TQueue q1;
	q1.Put(1);
	q1.Get();
	EXPECT_TRUE(q1.IsEmpty());
}

TEST(TQueue, IsEmpty_works_correctly)
{
	TQueue q1(3);
	EXPECT_TRUE(q1.IsEmpty()); 
}

TEST(TQueue, get_returns_first_placed_elem)
{
	TQueue q1;
	int elem1 = 1, elem2 = 2, elem3 = 3, res;
	q1.Put(elem1); q1.Put(elem2); q1.Put(elem3);
	res = q1.Get();
	EXPECT_TRUE(res == elem1);
}

TEST(TQueue, IsFull_works_correctly)
{
	TQueue q1(3);
	q1.Put(1);
	q1.Put(2);
	q1.Put(3);
	EXPECT_TRUE(q1.IsFull());
}

TEST(TQueue, cant_get_elem_when_queue_is_empty)
{
	TQueue q1(3);
	ASSERT_ANY_THROW(q1.Get());
}

TEST(TQueue, ring_buffer_works_correctly)
{
    TQueue q1(3);
    q1.Put(1);
    q1.Put(2);
    q1.Put(3);
    q1.Get();
    q1.Get();
    q1.Put(4);
    q1.Put(5);
    EXPECT_EQ(1, q1.Get() == 3 && q1.Get() == 4 && q1.Get() == 5);
}